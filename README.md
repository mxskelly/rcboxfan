# RC Box Fan

The goal of this project is to create a modification that can be done to generic 
box fans to allow for controlling them with 433MHz RF remote controls.

## project structure:

- `docs` for all documentation including PDF exports of schematics and/or 
    mechanical drawings
- `electrical` for all electronics-related files such as kicad project files, 
    simulations, pcb manufacturing files, etc.
- `img` for all images such as photos of the project
- `mechanical` for all mechanical files such as CAD files or STLs
- `software` for all software/firmware project files